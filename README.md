# API Gateway

Gateway of the CovidAlert application in Spring Boot.

## Getting started
To install and run this application :

1. Download the source code
2. In application.yml, change every uri according the the uri of the service you want to use
3. Run ApiGatewayApplication.java

<a name="collaboration"></a>
## Collaboration
* [Valentin Guyon](https://github.com/V2i)
* [Alexia Ognard](https://github.com/Alexiaognard)
* [Axel Canton](https://github.com/AxelCanton)


<a name="licence"></a>
## License
[MIT](https://choosealicense.com/licenses/mit/)
